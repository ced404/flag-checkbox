

// TODO: https://vuejs.org/v2/guide/list.html#key : vm.$set(vm.userProfile, 'age', 27) ?



// ## ROUTES

// 0. If using a module system (e.g. via vue-cli), import Vue and VueRouter
// and then call `Vue.use(VueRouter)`.

// 1. Define route components.
// These can be imported from other files
const Foo = { template: '<div>foo</div>' }
const Bar = { template: '<div>bar</div>' }

// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// `Vue.extend()`, or just a component options object.
// We'll talk about nested routes later.
const routes = [
	{ path: '/foo', component: Foo },
	{ path: '/bar', component: Bar }
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
	routes // short for `routes: routes`
})



// ## STORE

const store = new Vuex.Store({
	state: {
		message: '',

		groups: {
			"A": [],
			"B": [],
		},


		toggleList : [
			{
				"id": 1,
				"name": "France",
				"pressed": false,
				"order": 0,
			},
			{
				"id": 2,
				"name": "England",
				"pressed": false,
				"order": 0,
			},
			{
				"id": 3,
				"name": "Italy",
				"pressed": false,
				"order": 0,
			},
		],
	},

	mutations: {
		initialiseStore (state) {
			console.log('initialiseStore', localStorage.getItem ('toggleList'));
			if (! localStorage.getItem ('toggleList')) return;
			state.toggleList = JSON.parse (localStorage.getItem ('toggleList'));
		},
		updateToggleList (state, payload) {
			state.toggleList.find (x => x.id === payload.id).pressed = payload.pressed;
			let data = JSON.stringify (state.toggleList);
			localStorage.setItem ('toggleList', data);
		},
		updateMessage (state, payload) {
			console.log (payload.bodyText);
			state.message = payload.bodyText;
		}
	},

	getters: {
		loadToggleList (state) {
			return state.toggleList;
		},
		loadGroups (state) {
			return state.groups;
		}
	},

	actions: {
		refreshMessage (context) {
			return new Promise ((resolve) => {
				Vue.http.get ('https://jsonplaceholder.typicode.com/posts').then ((response) => {
					context.commit ('updateMessage', response);
					resolve (JSON.parse (response.bodyText));
				});
			});
		}
	},

});





// ## COMPONENTS

Vue.component ('toggle-button', {

	template: '#toggleButton',

	// default component state from attribute
	props: [
		'pressed',
		'id',
	],

	// component's data
	data: function () {
		return {
			// set initial state from attribute (props)
			'isPressed': this.pressed,
		}
	},

	// component's methods
	methods: {
		// toggle-button component state on/off
		// and emit event to parent when pressed
		handlePressed: function (value) {
			this.isPressed = !this.isPressed;
			let toggleState = {
				"id": this.id,
				"pressed": this.isPressed, // or "value"
			};
			this.$parent.$emit ('toggle-button-pressed', toggleState);
		},
	},

	/*
	watch: {
	// emit an event when "isPressed" changes
	isPressed: function (value, oldValue) {
	this.handlePressed (value);
},
}*/
});




// ## MAIN APP
var app = new Vue ({

	http: {
		root: '/',
		headers: {}
	},
	router,
	el: '#app',

	data: {
		toggleList: {},
		groups: {},
		message: 'empty',
	},

	created () {
		// get eventual previous state from localStorage
		store.commit ('initialiseStore');
		// load toggle-buttons
		this.toggleList = this.getToggles();
		this.groups = store.getters.loadGroups;
	},

	mounted () {
		// bind events
		// this.$on ('toggle-button-pressed', function (results) {
		// 	this.saveToggle (results);
		// });

		this.$on ('toggle-button-pressed', function (item) {

			// Gestion du Groupe

			// TODO: get group from item
			let g = 'A';

			// l'item dans le groupe (si déjà enregistré)
			let groupTeam = this.groups[g].find (it => it.id === item.id) || false;

			// item coché et n'est pas déjà enregistré ; on l'ajoute
			if (item.pressed && ! groupTeam) this.groups[g].push (item);

			// item décoché et déjà enregistré ; on le supprime
			if (! item.pressed && groupTeam) {
				let groupeTeamIndex = this.groups[g].findIndex (it => it.id === item.id);
				this.groups[g].splice (groupeTeamIndex, 1);
			}

			// le groupe est au max (2 items), on supprime le plus ancien (le premier du tableau)
			if (this.groups[g].length > 2) this.groups[g].shift();

			// mise à jour de la position du toggle dans le groupe
			this.updateToggleOrder (g);

			// gestion de l'état disabled des toggles
			if (this.groups[g].length == 2) this.disableToggles (g);
			else this.enableToggles (g)

			console.log ('group '+g, this.groups[g]);

			// TODO: 8èmes de finale : construire (v-for) les toggles
			// à partir de this.groups pour choisir les suivants, etc.
		});

	},


	methods: {

		updateToggleOrder: function (groupID) {
			[...this.toggleList].forEach (toggle => {
				let groupItem = this.groups[groupID].find (it => it.id === toggle.id) || false;
				let groupItemIndex = groupItem ? this.groups[groupID].findIndex (it => it.id === toggle.id) : false;
				let order = groupItem ? groupItemIndex + 1 : 0;
				toggle.order = order;
				groupItem.order = order; // mutates this.groups
			});
		},

		disableToggles (groupID) {
			[...this.toggleList].forEach (toggle => {
				// si le toggle n'est dans aucun groupe, on le désactive
				if (! this.groups[groupID].find (it => it.id === toggle.id)) toggle.disabled = true;
			});
		},

		enableToggles () {
			[...this.toggleList].forEach (toggle => {
				toggle.disabled = false;
			});
		},

		getToggles () {
			return store.getters.loadToggleList;
		},

		saveToggle (toggle) {
			console.log ('saveToggle', toggle);
			store.commit ('updateToggleList', toggle);
			this.refreshMessage ();
		},

		refreshMessage () {
			this.message = "loading…";
			store.dispatch('refreshMessage').then ((results) => {
				console.log('done refreshing message', results);
				this.message = results[0];
			});
		},
	},

	computed: {
		// limitedChecked: function () {
		// 	if (this.checked.length > 2) return this.checked.pop();
		// 	return this.checked;
		// }
	}


});




/*
const Users2 = {
el: '#app',

data() {
return {
userList: {},
selectedUser: null
}
},

mounted() {
this.userList = this.getUsers()
},

methods: {
setSelectedUser(user) {
this.selectedUser = user
},
getUsers() {
return {... userList}
}
},

components: {
'user-rating': Rating2
}
}
*/
